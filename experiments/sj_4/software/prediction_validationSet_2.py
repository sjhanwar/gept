# Perform validation of a model on an independent test set
# Date: 25/07/2015
# Author: Shalu Jhanwar

#!/usr/bin/python
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from StringIO import StringIO
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import fbeta_score
from sklearn.metrics import metrics
from sklearn.externals import joblib
import optparse
import warnings
warnings.filterwarnings("ignore")


def main(options, args):

    if len(sys.argv[1:]) == 0:
        print "no argument given!"
        print parser.print_help()
        sys.exit(2)
    if not options.data_file and options.test_file:
        print "Either test file or data-File is missing \n Please provide the file and run the program again\nAborting....\n"
        sys.exit(2)

    out_folder = options.output_folder
    data_cols = options.data_columns
    label_col = int(options.label_column)
    model_filename = os.path.abspath(options.model_file)
    data_file = os.path.abspath(options.data_file)
    test_file = os.path.abspath(options.test_file)

    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    if int(options.verbosity) > 0:
        print "Options: ", options
        print "Model file is", model_filename, "\n"

    data_cols = [int(x) for x in data_cols.split(",")]
    feature_data = np.loadtxt(data_file, usecols=data_cols, delimiter = "\t", skiprows=1)
    test_feature_data = np.loadtxt(test_file, usecols=data_cols, delimiter = "\t", skiprows=1)
    test_labels = np.genfromtxt(test_file,  usecols = label_col, delimiter = "\t", skiprows=1)
    print "No. of samples in the validation dataset: ", len(test_labels),"\n"
    #Load the model file#
    estimator = joblib.load(model_filename)

    #perform same scaling on training and testing data
    feature_data_scaled, test_feature_data_scaled = scaling_training_testing_data(feature_data, test_feature_data)
    np.random.seed(0)
    shuffled_indices = np.random.permutation(len(test_feature_data_scaled))
    test_feature_data_scaled = test_feature_data_scaled[shuffled_indices]
    test_labels = test_labels[shuffled_indices]
    cols = 0

    #Get enhancers names:
    with open (test_file,"r") as temp:
        a =  '\n'.join(line.strip("\n") for line in temp)
        b = np.genfromtxt(StringIO(a), usecols = cols, delimiter="\t", dtype=None, skiprows=1)
        enhancer_names_test = b[shuffled_indices]
    temp.close()
    y_pred = estimator.predict(test_feature_data_scaled)
    y_score_test = estimator.predict_proba(test_feature_data_scaled)
    combined_test_predictions = zip(enhancer_names_test, test_labels, y_pred, y_score_test[:,0], y_score_test[:,1])
    prediction_output= open(out_folder + "/Predictions_" + options.save_file + ".txt", 'w')
    prediction_output.write("Enhancer_name\tY_true_labels\tY_predicted_labels\tProb_Class0\tProb_class1\n")
    for i in combined_test_predictions:
        line = '\t'.join(str(x) for x in i)
        prediction_output.write(line + '\n')
    prediction_output.close()

    print "Classification report of the prediction is", metrics.classification_report(test_labels,y_pred), "\n"
    print "Random Forests: Final Generalization Accuracy: %.6f" %metrics.accuracy_score(test_labels,y_pred)
    print "Number of mislabeled samples : %d" % (test_labels != y_pred).sum()
    #Before we move on, let's look at a key parameter that RF returns, namely feature_importances. This tells us which #features in our dataset seemed to matter the most (although won't matter in the present scenario with only 2 features)
    if int(int(options.verbosity)) > 0:
        # Get names of the features from header of file
        infile = open(options.test_file, 'r')
        firstline = infile.readline().rstrip()
        names = [x for x in firstline.split("\t")]
        names_sel = []
        for i in data_cols:
            names_sel.append(names[i])
        print "names", names_sel,"\n"
        infile.close()
        # Get the indices of the features according to their importance
        feature_rank_descend = np.argsort(estimator.feature_importances_)[::-1]  # Descending Order
        for f in xrange(len(data_cols)):
            print "%d. feature %d (%f) %s" % (
            f + 1, feature_rank_descend[f], estimator.feature_importances_[feature_rank_descend[f]], names_sel[feature_rank_descend[f]])

    #Plot ROC#
    roc_plt = plot_roc(estimator, test_feature_data_scaled, test_labels, y_pred)
    roc_plt.savefig(out_folder + "/ROC-curve_" + options.save_file + ".svg", bbox_inches='tight', pad_inches=0.2)
    roc_plt.show()
    roc_plt.close()

def scaling_training_testing_data(train_data, test_data):
    scaler = preprocessing.StandardScaler()
    train_data = scaler.fit_transform(train_data)
    test_data = scaler.transform(test_data)
    return train_data, test_data

def plot_roc(estimator, test_x_data, test_y_data, y_pred):
    y_score = estimator.predict_proba(test_x_data)
    y_score =np.around(y_score, decimals=2)
    prec = precision_score(test_y_data, y_pred, average='micro')
    rec = recall_score(test_y_data, y_pred, average='micro')
    fscore = fbeta_score(test_y_data, y_pred, average='micro', beta=0.5)
    areaRoc = roc_auc_score(test_y_data, y_score[:,1])

    if int(int(options.verbosity)) > 0:
        print "Precision: ", round(prec, 3)
        print "Recall: ", round(rec, 3)
        print "F-measure: ", round(fscore, 3)
        print "AUC: ", round(areaRoc,3)
    #Generate ROC curve for each cross-validation
    fpr, tpr, thresholds = roc_curve(test_y_data, y_score[:,1], pos_label = 1)  #Pos level for positive class
    precision, recall, threshold = precision_recall_curve(test_y_data, y_score[:,1])
    random_mean_auc = auc(fpr, tpr)
    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
    plt.plot(fpr, tpr, 'k--',label='RF (area = %0.2f)' % random_mean_auc, lw=3, color=(0.45, 0.42, 0.18)) #Plot mean ROC area in cross validation
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic Curve')
    plt.legend(loc="lower right")
    return plt

if __name__ == "__main__":
    usage = "usage: %prog [required: --data-file <Filename> --feature-columns <feature column indices> --label-column <label column>] [optional: --output-folder <Output_folder> --fold-cross-validation <no. of cross-validation-folds> --save-file <output-filename> --n-estimators <value> --max-depth <value>]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("", "--data-file", dest="data_file", default="", help="A tab delimited file: 1st column as rownames, 2nd column as class and additional columns as tab-delimited features: see example folder for file format")
    parser.add_option("", "--output-folder", dest="output_folder", default="../output/", help="An output folder: Default is ../output")
    parser.add_option("", "--label-column", dest="label_column", default=1, help="Column index containing class of instances in test file: Default: 1 (2nd column)")
    parser.add_option("", "--feature-columns", dest="data_columns", default="2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17", help="Column index containing features in data-file")
    parser.add_option("", "--fold-cross-validation", dest="fold_cross_validation", default=10, help="n-fold: Default:10")
    parser.add_option("", "--save-file", dest="save_file", default="output_file", help="Output filename: Deafult: output_file")
    parser.add_option("", "--n-jobs", dest="n_jobs", default=10, help="no. of CPUs <value>")
    parser.add_option("", "--test_file", dest="test_file", default="", help="A test file: 1st column as rownames, 2nd column as class and additional columns as tab-delimited features: see example folder for test file")
    parser.add_option("", "--model_file", dest="model_file", default="", help="A file containing model")
    parser.add_option("", "--verbosity", dest="verbosity", default=1, help="Verbosity: Default on: provide 0 to turn off")
    (options, args) = parser.parse_args()
    main(options, args)
