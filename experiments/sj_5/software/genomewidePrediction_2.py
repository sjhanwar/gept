# Perform genomewide enhancer prediction
# Date: 25/07/2015
# Author: Shalu Jhanwar

#!/usr/bin/python
import sys
import numpy as np
from sklearn import preprocessing
from StringIO import StringIO
from sklearn.externals import joblib
import os
import optparse
import warnings
warnings.filterwarnings("ignore")

def main(options, args):

    if len(sys.argv[1:]) == 0:
        print "no argument given!"
        print parser.print_help()
        sys.exit(2)
    if not options.data_file and options.genome_file:
        print "Either genome region file or data-file is missing \n Please provide the file and run the program again\nAborting....\n"
        sys.exit(2)

    model_filename = os.path.abspath(options.model_file)
    data_file = os.path.abspath(options.data_file)
    genome_file = os.path.abspath(options.genome_file)
    out_folder = options.output_folder
    data_cols = options.data_columns

    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    if options.verbosity > 0:
        print "Options: ", options
        print "Model file is", model_filename, "\n"

    data_cols = [int(x) for x in data_cols.split(",")]
    feature_data = np.loadtxt(data_file, usecols=data_cols, delimiter = "\t", skiprows=1)
    genome_feature_data = np.loadtxt(genome_file, usecols=data_cols, delimiter = "\t", skiprows=1)

    #Load the model file#
    estimator = joblib.load(model_filename)

    #perform same scaling on training and testing data
    feature_data_scaled, genome_feature_data_scaled = scaling_training_testing_data(feature_data, genome_feature_data)
    np.random.seed(0)
    shuffled_indices = np.random.permutation(len(genome_feature_data_scaled))
    genome_feature_data_scaled = genome_feature_data_scaled[shuffled_indices]
    if options.verbosity > 0:
        print genome_feature_data.shape, ":genome_feature_data dimension\n"
        print genome_feature_data_scaled.shape, ":genome_feature_data_scaled dimension\n"
    cols = 0
    with open (genome_file,"r") as temp:
        a =  '\n'.join(line.strip("\n") for line in temp)
        b = np.genfromtxt(StringIO(a), usecols = cols, delimiter="\t", dtype=None, skiprows=1)
        enhancer_names_test = b[shuffled_indices]
    if options.verbosity > 0:
	    print enhancer_names_test.shape, "enhancer_names_test dimension"
    temp.close()

    y_pred = estimator.predict(genome_feature_data_scaled)
    y_score_test = estimator.predict_proba(genome_feature_data_scaled)
    combined_test = zip(enhancer_names_test, y_pred, y_score_test[:,0], y_score_test[:,1])
    prediction_output= open(out_folder + "/" + options.save_file + ".txt", 'w')
    prediction_output.write("Enhancer_name\tY_true_labels\tY_predicted_labels\tProb_Class0\tProb_class1\n")
    for i in combined_test:
        line = '\t'.join(str(x) for x in i)
        prediction_output.write(line + '\n')
    prediction_output.close()

def scaling_training_testing_data(train_data, test_data):
    scaler = preprocessing.StandardScaler()
    train_data = scaler.fit_transform(train_data)
    test_data = scaler.transform(test_data)
    return train_data, test_data

if __name__ == "__main__":
    usage = "usage: %prog [required: --data-file <Filename> --feature-columns <feature column indices> --label-column <label column>] [optional: --output-folder <Output_folder> --fold-cross-validation <no. of cross-validation-folds> --save-file <output-filename> --n-estimators <value> --max-depth <value>]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("", "--data-file", dest="data_file", default="", help="A tab delimited file: 1st column as rownames, 2nd column as class and additional columns as tab-delimited features: see example folder for file format")
    parser.add_option("", "--output-folder", dest="output_folder", default="../output/", help="An output folder: Default is ../output")
    parser.add_option("", "--feature-columns", dest="data_columns", default="2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17", help="Column index containing features in data-file")
    parser.add_option("", "--fold-cross-validation", dest="fold_cross_validation", default=10, help="n-fold: Default:10")
    parser.add_option("", "--save-file", dest="save_file", default="output_file", help="Output filename: Deafult: output_file")
    parser.add_option("", "--n-jobs", dest="n_jobs", default=10, help="No. of CPUs <value>")
    parser.add_option("", "--genome_file", dest="genome_file", default="", help="Genome-wide file: 1st column as rownames and additional columns as tab-delimited features: see example folder for test file")
    parser.add_option("", "--model_file", dest="model_file", default="", help="A file containing model")
    parser.add_option("", "--verbosity", dest="verbosity", default=1, help="Verbosity: Default on: provide 0 to turn off")
    (options, args) = parser.parse_args()
    main(options, args)


