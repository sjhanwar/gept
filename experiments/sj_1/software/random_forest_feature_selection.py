#! /usr/bin/env python

import sys
import getopt
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
import os

os.getcwd()

def main(argv):

    # get options passed at command line
	try:
		opts, args = getopt.getopt(argv, "d:o:c:C:")

	except getopt.GetoptError:
        #print helpString
		sys.exit(2)
#print opts
	for opt, arg in opts:

		if opt == '-d':
			data_file = arg #File with header
		elif opt == '-o':
			out_folder = arg
		elif opt == '-c':
			label_col = int(arg)
		elif opt == '-C':
			data_cols = arg

	if not os.path.exists(out_folder):
    		os.makedirs(out_folder)

	#Get columns indices of the features
	data_cols = [int(x) for x in data_cols.split(",")]

	#Get names of the features from header of file
	infile = open(data_file, 'r')
	firstline = infile.readline().rstrip()
	names = [x for x in firstline.split("\t")]
	del names[0] #delete column for position
	del names[0] #delete column for class
	infile.close() 

	#Read features and labels
	x_data = np.loadtxt(data_file, usecols=data_cols, delimiter = "\t", skiprows=1)
	y_data = np.genfromtxt(data_file, usecols = label_col, delimiter = "\t", skiprows = 1)
	
	#data_file = "/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Poeya_outputFolder/Poeya_matrix.txt"
	#data_file = "/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/K562_outputFolder/K562_encode_matrix.txt"
	#data_cols = "2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17"
	#data_cols = "2,3,4,5,6,7,8,9"
	#label_col = 1

	#Scaling of the data
	#min_max_scaler = preprocessing.MinMaxScaler()
	#x_data = min_max_scaler.fit_transform(x_data)
	
	#same Scaling on both test and train data (centering the data scaling)
	scaler = preprocessing.StandardScaler()
	x_data = scaler.fit_transform(x_data)

	#Shuffle rows of the data
	np.random.seed(0)
	shuffled_indices = np.random.permutation(len(x_data))
	My_X_train = x_data[shuffled_indices]
	My_Y_train = y_data[shuffled_indices]

	#With significant data after feature selection:
	#Provide importance of the feature
	clf_fea = RandomForestClassifier(n_estimators=100, random_state=0)
	#clf_fea = ExtraTreesClassifier(compute_importances=True, random_state=0,  n_estimators=100)
	clf_fea.fit(My_X_train, My_Y_train)

	#Importance of the feature is given by:
	importances = clf_fea.feature_importances_
	std = np.std([tree.feature_importances_ for tree in clf_fea.estimators_], axis = 0)

	#get the indices of the features according to their importance
	feature_rank_descend = np.argsort(importances)[::-1] #Descending Order

	# Print feature ranking
	for f in xrange(len(data_cols)):
	     print "%d. feature %d (%f)" % (f + 1, feature_rank_descend[f], importances[feature_rank_descend[f]])

	# Get Feature Importance from the classifier
	feature_rank_ascend = np.argsort(clf_fea.feature_importances_) #Ascending Order

	#Plot the feature importances of the forest
	#Plot the importance of the feature as bar plot
	print feature_rank_ascend
	print names
	print len(names)
	plt.barh(np.arange(len(names)), clf_fea.feature_importances_[feature_rank_ascend])
	plt.yticks(np.arange(len(names)) + 0.25, np.array(names)[feature_rank_ascend])
	_ = plt.xlabel('Relative importance')
	plt.savefig(out_folder + '/withNames_RFClassifier_Feature_importance.svg', bbox_inches='tight', pad_inches=0.2)
	plt.show()
	
	import pylab as pl
	pl.figure()
	pl.title("Feature Importances")
	pl.bar(xrange(len(data_cols)), importances[feature_rank_descend], color="r", yerr=std[feature_rank_descend], align="center")
	pl.xticks(xrange(len(data_cols)), feature_rank_descend)
	pl.xlim([-1, len(data_cols)])
	#pl.savefig('/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Poeya_Feature_importance_RF.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	#pl.savefig('/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Feature_importance_RF_FANTOM.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	pl.savefig(out_folder + '/RF_classifier_tssDist_include_Feature_importance.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	pl.savefig(out_folder + '/RF_classifier_No_transperant_tssDist_include_Feature_importance.svg', bbox_inches='tight', pad_inches=0.2)
	pl.show()

	# Plot the feature importances of the trees and of the forest
	import pylab as pl
	pl.figure()
	pl.title("Feature Importances")

	for tree in clf_fea.estimators_:
	    pl.plot(xrange(len(data_cols)), tree.feature_importances_[feature_rank_descend], "r")

	pl.plot(xrange(len(data_cols)), importances[feature_rank_descend], "b")
	#pl.savefig(out_folder + '/tssDist_include_Feature_importanceline.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	#pl.savefig('/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Feature_importance_RF_FANTOM_line.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	#pl.savefig('/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Poeya_Feature_importance_RF_line.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	pl.show()

	#Select only those features which are important by leaving other features
	My_X_train = clf_fea.fit(My_X_train, My_Y_train).transform(My_X_train)
	print "The shape of the data after feature selection is", My_X_train.shape, "\n"

if __name__ == "__main__":
	main(sys.argv[1:])

