# Perform feature selection using Random Forest classifier
# Date: 5/03/2015
# Author: Shalu Jhanwar

#!/usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
import os
from optparse import OptionParser

def main(options, args):

    out_folder = options.output_folder
    data_cols = options.data_columns
    label_col = int(options.label_column)

    if not options.data_file:
        print "No Data File Present \n Aborting....."
        sys.exit(2)
    if options.verbosity > 0:
        print "Label Column: ", label_col
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    # Get columns indices of the features
    data_cols = [int(x) for x in data_cols.split(",")]

    # Get names of the features from header of file
    infile = open(options.data_file, 'r')
    firstline = infile.readline().rstrip()
    names = [x for x in firstline.split("\t")]
    del names[0]  # delete column for position
    del names[0]  # delete column for class
    infile.close()

    # Read features and labels
    x_data = np.loadtxt(options.data_file, usecols=data_cols, delimiter="\t", skiprows=1)
    y_data = np.genfromtxt(options.data_file, usecols=label_col, delimiter="\t", skiprows=1)

    # Same scaling on both test and train data (centering the data scaling)
    scaler = preprocessing.StandardScaler()
    x_data = scaler.fit_transform(x_data)

    # Shuffle rows of the data
    np.random.seed(0)
    shuffled_indices = np.random.permutation(len(x_data))
    My_X_train = x_data[shuffled_indices]
    My_Y_train = y_data[shuffled_indices]

    # With significant data after feature selection:Provide importance of the feature
    clf_fea = RandomForestClassifier(n_estimators=int(options.n_estimators), random_state=0)
    clf_fea.fit(My_X_train, My_Y_train)

    # Importance of the feature is given by:
    importances = clf_fea.feature_importances_
    if options.verbosity > 0:
        print "Feature Importance:", importances

    # Get the indices of the features according to their importance
    feature_rank_descend = np.argsort(importances)[::-1]  # Descending Order

    # Print feature ranking
    if options.verbosity > 0:
        for f in xrange(len(data_cols)):
            print "%d. feature %d (%f) %s" % (
            f + 1, feature_rank_descend[f], importances[feature_rank_descend[f]], names[feature_rank_descend[f]])

    # Get Feature Importance from the classifier
    feature_rank_ascend = np.argsort(clf_fea.feature_importances_)  # Ascending Order

    # Plot the importance of the feature as bar plot
    if int(options.plot_bar_without_std) == 1:
        plt.barh(np.arange(len(names)), clf_fea.feature_importances_[feature_rank_ascend])
        plt.yticks(np.arange(len(names)) + 0.25, np.array(names)[feature_rank_ascend])
        _ = plt.xlabel('Relative importance')
        plt.savefig(out_folder + '/withNames_RFClassifier_Feature_importance.svg', bbox_inches='tight', pad_inches=0.2)
        plt.show()
        plt.close()

    if int(options.plot_bar_with_std) == 1:
        std = np.std([feature.feature_importances_ for feature in clf_fea.estimators_], axis=0)
        plt.figure()
        plt.title("Feature Importances")
        plt.bar(xrange(len(data_cols)), importances[feature_rank_descend], color="r", yerr=std[feature_rank_descend],
                align="center")
        plt.xticks(xrange(len(data_cols)), feature_rank_descend)
        plt.xlim([-1, len(data_cols)])
        plt.savefig(out_folder + '/RF_classifier_tssDist_include_Feature_importance.svg', transparent=True,
                    bbox_inches='tight', pad_inches=0.2)
        # pl.savefig(out_folder + '/RF_classifier_No_transperant_tssDist_include_Feature_importance.svg', bbox_inches='tight', pad_inches=0.2)
        plt.show()
        plt.close()

    # Plot the feature importances of the trees and of the forest
    if int(options.plot_line) == 1:
        plt.figure()
        plt.title("Feature Importances")

        for feature in clf_fea.estimators_:
            plt.plot(xrange(len(data_cols)), feature.feature_importances_[feature_rank_descend], "r")

        plt.plot(xrange(len(data_cols)), importances[feature_rank_descend], "b")
        plt.show()
        plt.close()

    # Select only those features which are important by leaving other features
    My_X_train = clf_fea.fit(My_X_train, My_Y_train).transform(My_X_train)
    if options.verbosity > 0:
        print "The shape of the data after feature selection is", My_X_train.shape, "\n"


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("", "--data-file", dest="data_file", default="", help="A tab delimited file: 1st column as rownames, 2nd column as class and additional columns as tab-delimited features: see example folder for test file")
    parser.add_option("", "--output-folder", dest="output_folder", default="../output/", help="An output folder: Default is ../output")
    parser.add_option("", "--label-column", dest="label_column", default=1, help="Column index containing class of instances: Default: 1 (2nd column)")
    parser.add_option("", "--feature-columns", dest="data_columns", default="2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17", help="Column index containing features in data-file")
    parser.add_option("", "--plot-bar-without-std", dest="plot_bar_without_std", default=1, help="Bar plot without std deviation: Default:on")
    parser.add_option("", "--plot-bar-with-std", dest="plot_bar_with_std", default=0, help="Bar plot with std deviation: Default:off, provide 1 value to turn on")
    parser.add_option("", "--plot-line", dest="plot_line", default=0, help="Line plot: Default off: provide 1 value to turn on")
    parser.add_option("", "--n-estimators", dest="n_estimators", default=int(100), help="n_estimator: Default=100")
    parser.add_option("", "--verbosity", dest="verbosity", default=1, help="Verbosity: Default on: provide 0 to turn off")
    (options, args) = parser.parse_args()
    main(options, args)
