#!/usr/bin/python

import sys
import getopt
import numpy as np
import pylab as pl
import random
from sklearn import datasets
from sklearn.learning_curve import learning_curve
from scipy import interp
import matplotlib.pyplot as plt
from scipy import interp
from sklearn import svm
from sklearn import preprocessing
from sklearn import linear_model
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.svm import SVC
from StringIO import StringIO
from sklearn.metrics import roc_auc_score
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import fbeta_score
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import metrics
from sklearn.naive_bayes import GaussianNB
from sklearn.datasets import load_digits
from sklearn.externals import joblib
from sklearn.cross_validation import StratifiedShuffleSplit
import os
os.getcwd()

def main(argv):

    # get options passed at command line
    try:
        opts, args = getopt.getopt(argv, "d:o:c:C:")
    except getopt.GetoptError:
            #print helpString
        sys.exit(2)
    #print opts
    for opt, arg in opts:
        if opt == '-d':
            data_file = arg
        elif opt == '-o':
            out_folder = arg
        elif opt == '-c':
            label_col = int(arg)
        elif opt == '-C':
            data_cols = arg

	if not os.path.exists(out_folder):
    		os.makedirs(out_folder)

    data_cols = [int(x) for x in data_cols.split(",")]
    x_data = np.loadtxt(data_file, usecols=data_cols, delimiter = "\t", skiprows = 1)
    y_data = np.genfromtxt(data_file,  usecols = label_col, delimiter = "\t", skiprows = 1)
    #test_x_data = np.loadtxt(test_file, usecols=data_cols, delimiter = "\t", skiprows = 1)
    #test_y_data = np.genfromtxt(test_file,  usecols = label_col, delimiter = "\t", skiprows = 1)
    #genome_data = np.loadtxt(genome_file, usecols=data_cols, delimiter = "\t", skiprows = 1)
#perform same scaling on training and testing data
    x_data = scaling_training_testing_data(x_data)
    np.random.seed(0)
    indices = np.random.permutation(len(x_data))
    x_data = x_data[indices]
    y_data = y_data[indices]

#Train and test on the whole dataset
    whole_dataset_train_test(x_data, y_data)

#Get indices of 2 classes seperately to make a test dataset with 20% split in the whole dataset in straified manner
    X_train, y_train, X_test, y_test = make_training_testing_data(x_data, y_data, indices)
#Make cross-validation iterator to tune the parameters by taking 20% of the remaining 80% of the training data using STRAITIFIED shuffled 10 fold cross-validation
    cv = StratifiedShuffleSplit(y_train, 10, test_size=0.2, random_state=0)
#Apply the cross-validation iterator on the Training set using GridSearchCV
    print "cv is", cv
    best_max_depth, best_n_estimators = grid_searchCV(cv, X_train, y_train)
    print "Best parameters are: max_depth =", best_max_depth, "n_estimator:", best_n_estimators, '\n' 
    #Fit estimators with the best parameters
    estimator = RandomForestClassifier(n_estimators=best_n_estimators,max_depth=best_max_depth,random_state=0)
#Below is a plot_learning_curve module that's provided by scikit-learn. It allows us to quickly and easily visualize how #well the model is performing based on number of samples we're training 		on. It helps to understand situations such as  #high variance or bias.
    print "See how well the model is fitting: plot learning curve for testing and training dataset\n";
    title = "Learning Curves (Random Forests, n_estimators=%.6f)" %(best_n_estimators)
    plot_learning_curve(estimator, title, X_train, y_train, cv)
    pl.savefig(out_folder + "/HepG2_training_manu_outputOnly_learning.svg", bbox_inches='tight', pad_inches=0.2)
    plt.show()

#Let's see how our trained model performs on the test set. We are not going to train on this set merely looking at how well our model can generalize.
    estimator.fit(X_train, y_train)
    #Save the model to load afterwards#
    filename = out_folder + "/test.pkl"
    joblib.dump(estimator, filename, compress=9)
    y_pred=estimator.predict(X_test)
    roc_plt = plot_roc(estimator, X_test, y_test, y_pred)
    pl.savefig(out_folder + "/HepG2_training_manu_outputOnly_HepG2_RF_FeatureSelected_split_test_train_Kfold.svg", bbox_inches='tight', pad_inches=0.2)
    roc_plt.show()

    print metrics.classification_report(y_test,y_pred)
    print "Random Forests: Final Generalization Accuracy: %.6f" %metrics.accuracy_score(y_test,y_pred)
    #Before we move on, let's look at a key parameter that RF returns, namely feature_importances. This tells us which #features in our dataset seemed to matter the most (although won't matter in the present scenario with only 2 features)
    print estimator.feature_importances_


###Defining all the subroutines###
def scaling_training_testing_data(train_data):
    #Scaling of the data
    #min_max_scaler = preprocessing.MinMaxScaler()
    #x_data = min_max_scaler.fit_transform(x_data)
    
    #same Scaling on both test and train data (centering the data scaling)
    scaler = preprocessing.StandardScaler()
    train_data = scaler.fit_transform(train_data)
    #test_data = scaler.transform(test_data)
    return train_data

def whole_dataset_train_test(X, y):
    rfpred = RandomForestClassifier().fit(X,y)
    pred = rfpred.predict(X)
    print "When fitted on the whole dataset with selected features, then the classification report is found to be:\n";
    print "Random Forests: Accuracy: %.6f" %metrics.accuracy_score(y,pred)
    print metrics.classification_report(y, pred)

def make_training_testing_data(Xtrain, Ytrain, indices):
    indices_1 = indices[Ytrain[indices]!=0]
    indices_0 = indices[Ytrain[indices]!=1]
    n_test = round((indices.size*.20)/2) #No. of instances for each class #SHUFFLE DATA X and Y for splitting purpose
    n_test = int(n_test)
    print "The total no. of instances in straitified test samples are:", n_test ,"\n"
    test_indices=np.concatenate([(indices_1[:n_test]), (indices_0[:n_test])])
    np.random.shuffle(test_indices)
    train_indices=np.concatenate([(indices_1[n_test:]), (indices_0[n_test:])])
    np.random.shuffle(train_indices)
    X_test = Xtrain[test_indices]
    y_test = Ytrain[test_indices]
    X_train = Xtrain[train_indices]
    y_train = Ytrain[train_indices]
    print X_test.shape, y_test.shape, X_train.shape , y_train.shape
    return X_train, y_train, X_test, y_test

def grid_searchCV(cv, X_train, y_train):
    max_depth=np.linspace(5,10,5) #By default
    n_estimators=[10, 100, 1000] #n_estimator
    n_jobs=3 #no_of_CPU
    clf = RandomForestClassifier(random_state=0)
    classifier = GridSearchCV(estimator=clf, cv=cv, param_grid=dict(n_estimators=n_estimators, max_depth=max_depth), n_jobs=n_jobs, scoring='f1')
    #Also note that we're feeding multiple neighbors to the GridSearch to try out. #We'll now fit the training dataset to this classifier
    
    classifier.fit(X_train, y_train)
    #Let's look at the best estimator that was found by GridSearchCV
    print "Best Estimator learned through GridSearch for random forest are:"
    print classifier.best_estimator_ ,"\n";
    max_depth = classifier.best_estimator_.max_depth #bEST MAX_DEPTH
    n_estimators = classifier.best_estimator_.n_estimators #Best n_estimator
    return classifier.best_estimator_.max_depth, classifier.best_estimator_.n_estimators

def plot_learning_curve(estimator, title, X_train, y_train, cv, ylim=None,  n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
    print "cv value is", cv
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(estimator, X_train, y_train, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()
    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,train_scores_mean + train_scores_std,alpha=0.1,color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",label="Cross-validation score")
    plt.legend(loc="best")
    return plt


def plot_roc(estimator, X_test, y_test, y_pred):
    y_score = estimator.predict_proba(X_test)
    y_score =np.around(y_score, decimals=2)
    
    accurate = accuracy_score(y_test, y_pred)
    print "Accuracy All dataset: ", accurate
    prec = precision_score(y_test, y_pred, average='micro')
    rec = recall_score(y_test, y_pred, average='micro')
    fscore = fbeta_score(y_test, y_pred, average='micro', beta=0.5)
    areaRoc = roc_auc_score(y_test, y_score[:,1])
    
    #Generate ROC curve for each cross-validation
    fpr, tpr, thresholds = roc_curve(y_test, y_score[:,1], pos_label = 1)  #Pos level for positive class
    precision, recall, threshold = precision_recall_curve(y_test, y_score[:,1])
    random_mean_auc = auc(fpr, tpr)
    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
    plt.plot(fpr, tpr, 'k--',label='RF_ROC_all_data (area = %0.2f)' % random_mean_auc, lw=2, color=(0.45, 0.42, 0.18)) #Plot mean ROC area in cross validation
#plt.plot(fpr_FS, tpr_FS, 'k--',label='Random Forest (area = %0.2f)' % random_mean_auc, lw=2, color=(0.93, 0.12, 0.78)) #Plot mean ROC area in cross validation
#plt.plot(fpr_FS, tpr_FS, 'k--',label='SVM (area = %0.2f)' % random_mean_auc, lw=2, color=(0.43, 0.82, 0.68)) #Plot mean ROC area in cross validation
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    return plt


if __name__ == "__main__":
    main(sys.argv[1:])


