# Perform model training using Random Forest classifier
# Date: 25/07/2015
# Author: Shalu Jhanwar

#!/usr/bin/python
import sys
import numpy as np
from sklearn.learning_curve import learning_curve
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import metrics
from sklearn.externals import joblib
from sklearn.cross_validation import StratifiedShuffleSplit
import os
import optparse
import pickle

def main(options, args):
    if len(sys.argv[1:]) == 0:
        print "no argument given!"
        print parser.print_help()
        sys.exit(2)
    if not options.data_file:
        print "No Data File Present \n Aborting....."
        sys.exit(2)
    out_folder = options.output_folder
    data_cols = options.data_columns
    label_col = int(options.label_column)
    if int(options.verbosity) > 0:
        print "Options: ", options

    if options.n_estimators:
        n_estimators = [int(x) for x in options.n_estimators.split(",")]

    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    # Get columns indices of the features
    data_cols = [int(x) for x in data_cols.split(",")]

    # Read features and labels
    feature_data = np.loadtxt(options.data_file, usecols=data_cols, delimiter = "\t", skiprows = 1)
    labels = np.genfromtxt(options.data_file,  usecols=label_col, delimiter = "\t", skiprows = 1)

    #Perform same scaling on training and testing data
    scaler = preprocessing.StandardScaler()
    feature_data_scaled = scaler.fit_transform(feature_data)
    scalerData = options.output_folder + "/" + "Scalar_" + options.save_file + ".pkl"

    with open(scalerData, 'wb') as fin_save_pos:
        pickle.dump(scaler, fin_save_pos)

    np.random.seed(0)
    shuffled_indices = np.random.permutation(len(feature_data_scaled))
    feature_data_shuffled = feature_data_scaled[shuffled_indices]
    labels_shuffled = labels[shuffled_indices]

    #Train and test on the whole dataset
    sanity_check(feature_data_shuffled, labels_shuffled, int(options.verbosity))

    #Get indices of 2 classes seperately to make a test dataset with 20% split in the whole dataset in straified manner
    features_train, labels_train, features_test, labels_test = make_train_test_data(feature_data_shuffled, labels_shuffled, shuffled_indices, float(options.percent_test_size), int(options.verbosity))

    #Make cross-validation iterator to tune the parameters by taking 20% of the remaining 80% of the training data using STRAITIFIED shuffled 10 fold cross-validation
    cross_validation_object = StratifiedShuffleSplit(labels_train, int(options.fold_cross_validation), test_size=float(options.percent_test_size), random_state=0)

    #Apply the cross-validation iterator on the Training set using GridSearchCV
    if int(options.verbosity) > 0:
        print "Cross Validation is: ", cross_validation_object

    best_max_depth, best_n_estimators = grid_searchCV(cross_validation_object, features_train, labels_train, n_estimators, int(options.max_depth_start), int(options.max_depth_end), int(options.n_jobs), int(options.verbosity))
    if int(options.verbosity) > 0:
        print "Best parameters are: \n max_depth = ", best_max_depth, "\n n_estimator = ", best_n_estimators, '\n'

    #Fit estimators with the best parameters
    estimator = RandomForestClassifier(n_estimators=best_n_estimators, max_depth=best_max_depth, random_state=0)

    #Below is a plot_learning_curve module that's provided by scikit-learn. It allows us to quickly and easily visualize how #well the model is performing based on number of samples we're training on. It helps to understand situations such as  #high variance or bias.
    if int(options.verbosity) > 0:
        print "See how well the model is fitting - plot learning curve for testing and training dataset\n";
    title = "Learning Curves (Random Forests, n_estimators=%.6f)" %(best_n_estimators)
    learning_plot=plot_learning_curve(estimator, title, features_train, labels_train, cross_validation_object, int(options.verbosity))
    learning_curve_figure_file = options.output_folder + "/" + "Learning_Curve_" + options.save_file + ".svg"
    learning_plot.savefig(learning_curve_figure_file, bbox_inches='tight', pad_inches=0.2)
    learning_plot.show()
    learning_plot.close()
    #Let's see how our trained model performs on the test set. We are not going to train on this set merely looking at how well our model can generalize.
    estimator.fit(features_train, labels_train)

    #Save the model to load afterwards
    filename_model = options.output_folder + "/" + "Model_" + options.save_file + ".pkl"
    joblib.dump(estimator, filename_model, compress=9)
    #Predict on the test set
    predicted_labels = estimator.predict(features_test)

    #Plot ROC Curve
    roc_plt = plot_roc(estimator, features_test, labels_test, predicted_labels, int(int(options.verbosity)))
    ROC_curve_figure_file = options.output_folder + "/" + "ROC_Curve_" + options.save_file + ".svg"
    roc_plt.savefig(ROC_curve_figure_file, bbox_inches='tight', pad_inches=0.2)
    roc_plt.show()
    roc_plt.close()
    if int(options.verbosity) > 0:
        print metrics.classification_report(labels_test,predicted_labels)
        print "Random Forests: Final Generalization Accuracy: %.6f" %metrics.accuracy_score(labels_test,predicted_labels)
        print estimator.feature_importances_

def sanity_check(X, y, verbosity):
    RF_classifier = RandomForestClassifier().fit(X,y)
    pred = RF_classifier.predict(X)
    if verbosity > 0:
        print "When fitted on the whole dataset with selected features, then the classification report is found to be:\n";
        print metrics.classification_report(y, pred)
        print "Random Forests: Accuracy: %.6f" %metrics.accuracy_score(y,pred)


def make_train_test_data(Xtrain, Ytrain, indices, percentage_select, verbosity):
    indices_1 = indices[Ytrain[indices]!=0]
    indices_0 = indices[Ytrain[indices]!=1]
    n_test = round((indices.size*percentage_select)/2) #No. of instances for each class #SHUFFLE DATA X and Y for splitting purpose
    n_test = int(n_test)
    if verbosity > 0:
        print "The total no. of instances in each class is:", n_test ,"\n"

    test_indices = np.concatenate([(indices_1[:n_test]), (indices_0[:n_test])])
    np.random.shuffle(test_indices)
    train_indices = np.concatenate([(indices_1[n_test:]), (indices_0[n_test:])])
    np.random.shuffle(train_indices)
    X_test = Xtrain[test_indices]
    y_test = Ytrain[test_indices]
    X_train = Xtrain[train_indices]
    y_train = Ytrain[train_indices]
    if verbosity > 0:
        print "Train and Test Data Dimensions: ", X_test.shape, y_test.shape, X_train.shape , y_train.shape

    return X_train, y_train, X_test, y_test

def grid_searchCV(cv, X_train, y_train, n_estimators, max_depth_start, max_depth_end, n_jobs, verbosity):
    max_depth = np.linspace(max_depth_start, max_depth_end, 5) #By default
    n_estimators = n_estimators #n_estimator
    n_jobs = n_jobs #no_of_CPU
    clf = RandomForestClassifier(random_state=0)
    classifier = GridSearchCV(estimator=clf, cv=cv, param_grid=dict(n_estimators=n_estimators, max_depth=max_depth), n_jobs=n_jobs, scoring='f1')
    classifier.fit(X_train, y_train)
    #Let's look at the best estimator that was found by GridSearchCV
    if verbosity > 0:
        print "Best Estimator learned through GridSearch for random forest are: "
        print classifier.best_estimator_ ,"\n";
    return classifier.best_estimator_.max_depth, classifier.best_estimator_.n_estimators

def plot_learning_curve(estimator, title, X_train, y_train, cv, verbosity):
    ylim=None
    n_jobs=1
    train_sizes=np.linspace(0.1, 1.0, 5)
    if verbosity > 0:
        print "Cross Validation Value is ", cv
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(estimator, X_train, y_train, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes, scoring='f1')
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()
    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,train_scores_mean + train_scores_std,alpha=0.1,color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",label="Cross-validation score")
    plt.legend(loc="best")
    return plt

def plot_roc(estimator, X_test, y_test, y_pred, verbosity):
    y_score = estimator.predict_proba(X_test)
    y_score = np.around(y_score, decimals=2)
    if verbosity > 0:
        accurate = accuracy_score(y_test, y_pred)
        print "Accuracy All dataset: ", accurate
    #Generate ROC curve for each cross-validation
    fpr, tpr, thresholds = roc_curve(y_test, y_score[:,1], pos_label = 1)  #Pos level for positive class
    random_mean_auc = auc(fpr, tpr)
    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
    plt.plot(fpr, tpr, 'k--',label='Random Forest (area = %0.2f)' % random_mean_auc, lw=2, color=(0.45, 0.42, 0.18)) #Plot mean ROC area in cross validation
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic Curve')
    plt.legend(loc="lower right")
    return plt

if __name__ == "__main__":
    usage = "usage: %prog [required: --data-file <Filename> --feature-columns <feature column indices> --label-column <label column>] [optional: --output-folder <Output_folder> --fold-cross-validation <no. of cross-validation-folds> --percent-test-size <float> --save-file <output-filename> --n-estimators <value> --max-depth-start <value> --max-depth-end <value>--n-jobs <int>]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("", "--data-file", dest="data_file", default="", help="A tab delimited file: 1st column as rownames, 2nd column as class and additional columns as tab-delimited features: see example folder for test file")
    parser.add_option("", "--output-folder", dest="output_folder", default="../output/", help="An output folder: Default is ../output")
    parser.add_option("", "--label-column", dest="label_column", default=1, help="Column index containing class of instances: Default: 1 (2nd column)")
    parser.add_option("", "--feature-columns", dest="data_columns", default="2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17", help="Column index containing features in data-file")
    parser.add_option("", "--percent-test-size", dest="percent_test_size", default=0.20, help="Size of the test dataset in percentage: Default: 20")
    parser.add_option("", "--fold-cross-validation", dest="fold_cross_validation", default=10, help="n-fold: Default:10")
    parser.add_option("", "--save-file", dest="save_file", default="output_file", help="Output filename: Deafult: output_file")
    parser.add_option("", "--n-estimators", dest="n_estimators", default=[10, 100, 1000], help="n_estimator list: Default: [10, 100, 1000]")
    parser.add_option("", "--max-depth-start", dest="max_depth_start", default=5, help="max-depth start: Default:5")
    parser.add_option("", "--max-depth-end", dest="max_depth_end", default=10, help="max-depth end: Default: 10")
    parser.add_option("", "--n-jobs", dest="n_jobs", default=10, help="no. of CPUs: Default:10")
    parser.add_option("", "--verbosity", dest="verbosity", default=1, help="Verbosity: Default on: provide 0 to turn off")
    (options, args) = parser.parse_args()
    main(options, args)