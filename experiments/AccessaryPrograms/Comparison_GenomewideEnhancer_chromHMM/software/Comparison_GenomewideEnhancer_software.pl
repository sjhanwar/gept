#Check distribution of genome-wide predicted enhancers along with other softwares
#Date: 5/03/2015
#Author: Shalu Jhanwar

#!/usr/bin/perl
use strict;
use warnings;

if(!@ARGV){
	print "Usage: ###############################################################################################\nThis is a program to see the distribution of genome-wide predicted enhancers with the regions of other softwares\nInputs are: i) Predicted enhancer bed files \nii) A tab-delimited list with two cloumns (filename, region) of bed files of all the states/regions of software IN THE ORDER OF their preferences\niii) OutputFolder\n####################################################################################################\n";
		exit;
}

#Define variables
my $enhancerFile = $ARGV[0];
my $list = $ARGV[1];
my $output_folder = $ARGV[2];
print "$output_folder .. $enhancerFile\n";
`mkdir $output_folder/compTemp`;
#my $nEnhancer= `wc -l $enhancerFile | cut -d" " -f1`;
my $nEnhancer= `wc -l $enhancerFile | awk '{print \$1}'`;
print "Total no. of enhancers are:", $nEnhancer, "\n";
#Open region file
open(LIST, $list) || die "cant open file";
my $stringOverlap = ""; 
my $flag=0;
my $sum=0;

while(defined(my $_=<LIST>)){
	chomp($_);
	#print "$_\n";
	$flag++;

	my @s=split(/\t/,$_);
	if($flag==1){
	    print "$flag hiii \n";exit;
		print "intersectBed -a $enhancerFile -b $s[0] -wo | cut -f1,2,3 | sort | uniq > $output_folder/compTemp/temp_$s[1]\n";
		#`intersectBed -a $enhancerFile -b $s[0] -wo | cut -f1,2,3 | sort | uniq > $output_folder/compTemp/temp_$s[1]`;
		my $val=`wc -l $output_folder/compTemp/temp_$s[1] | cut -d" " -f1`;
		chomp($val);
		$sum = $sum + $val;
		print "Overlap with $s[1] is $val \n";exit;
		my $stringOverlap = $stringOverlap." ".$val;
		`cat $enhancerFile $output_folder/compTemp/temp_$s[1] | sort | uniq -u > $output_folder/compTemp/remainFile.bed`;
		
	}
	else{
		`intersectBed -a $output_folder/compTemp/remainFile.bed -b $s[0] -wo | cut -f1,2,3 | sort | uniq > $output_folder/compTemp/temp_$s[1]`;
		my $val=`wc -l $output_folder/compTemp/temp_$s[1] | cut -d" " -f1`;
		chomp($val);	
		$sum = $sum + $val;	
		print "Overlap with $s[1] is $val \n";
		my $stringOverlap = $stringOverlap." ".$val;
		`cat $output_folder/compTemp/remainFile.bed $output_folder/compTemp/temp_$s[1] | sort | uniq -u > $output_folder/compTemp/remain`;
		`mv $output_folder/compTemp/remain $output_folder/compTemp/remainFile.bed`;
	}
}

#Calculate No overlaps#
my $header = `cat $list | cut -f2 | tr '\n' '\t' | sed 's/\$/No_overlap/'`;
my $noOverlap = $nEnhancer - $sum;
$stringOverlap = $stringOverlap." ".$noOverlap;
print "$header", "\n", "$stringOverlap";
#Plot the bar chart#



	

