#!/usr/bin/python
import os
import sys
import getopt
import numpy as np
import pylab as pl
import random
from sklearn import datasets
from scipy import interp
from scipy.stats import sem
import matplotlib.pyplot as plt
from scipy import interp
from sklearn import preprocessing
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import fbeta_score
from sklearn.ensemble import ExtraTreesClassifier

os.getcwd()

def main(argv):
# get options passed at command line
	try:
		opts, args = getopt.getopt(argv, "d:o:c:C:")

	except getopt.GetoptError:

#print helpString

		sys.exit(2)
#print opts
	for opt, arg in opts:

		if opt == '-d':

			data_file = arg

		elif opt == '-o':

			out_folder = arg

		elif opt == '-c':

			label_col = int(arg)

		elif opt == '-C':

			data_cols = arg


	data_cols = [int(x) for x in data_cols.split(",")]
	x_data = np.loadtxt(data_file, usecols=data_cols, delimiter = "\t", skiprows = 1)
	y_data = np.genfromtxt(data_file,  usecols = label_col, delimiter = "\t", skiprows = 1)
	

	#Scaling of the data
	#min_max_scaler = preprocessing.MinMaxScaler()
	#x_data = min_max_scaler.fit_transform(x_data)
	
	#same Scaling on both test and train data (centering the data scaling)
	scaler = preprocessing.StandardScaler()
	x_data = scaler.fit_transform(x_data)

	np.random.seed(0)
	indices = np.random.permutation(len(x_data))
	My_X_train = x_data[indices]
	My_Y_train = y_data[indices]

	scores = list()   
	train_score = list()  #train_score
	test_score=list()    #test_score
	accurate = list()
	prec = list()
	rec = list()
	areaRoc = list()
	fscore = list()
	#For random classification
	accurate_rand = list()
	recRand = list()
	precRand = list()
	fscoreRand = list()
	areaRocRand = list()

	##For Random Forest, n_estimator=10
	random_mean_tpr_10 = 0.0
	random_mean_fpr_10 = np.linspace(0, 1, len(x_data))
	random_all_tpr_10 = []
	#k=1
	random.seed(80)
	skf=cross_validation.StratifiedKFold(My_Y_train, n_folds=10)
    	for train_index, test_index in skf:
		rand_list = list()
		X_train, X_test = My_X_train[train_index], My_X_train[test_index]
		y_train, y_test = My_Y_train[train_index], My_Y_train[test_index]
		print X_train, X_test, "\n" 
	
		clf = RandomForestClassifier(n_estimators=100, max_depth=5.0) #See the best estimator value from split train and test program
		scores.append(clf.fit(X_train, y_train).score(X_test, y_test))
		
		y_pred = clf.predict(X_test)
		print "y_pred", y_pred
		
		y_score = clf.predict_proba(X_test)
		y_score =np.around(y_score, decimals=2)
		accurate.append(accuracy_score(y_test, y_pred))
		#print "accurate_rand", accurate_rand
    	 #Calculate precision, recall, fscore For each cross-validation (classification + Random classification)
		#random.seed(k)
	    	for i in range(0,len(y_pred)):
			rand_list.append(random.randint(0, 1))
	   	y_rand = np.array(rand_list)
		accurate_rand.append(accuracy_score(y_test, y_rand))
	    #print "accurate_rand", accurate_rand
	    #Calculate precision, recall, fscore For each cross-validation (classification + Random classification)
	    	prec.append(precision_score(y_test, y_pred, average='micro'))
	    	rec.append(recall_score(y_test, y_pred, average='micro'))
	    	fscore.append(fbeta_score(y_test, y_pred, average='micro', beta=0.5))
	    	precRand.append(precision_score(y_test, y_rand, average='micro'))
	   	recRand.append(recall_score(y_test, y_rand, average='micro'))
	    	fscoreRand.append(fbeta_score(y_test, y_rand, average='micro', beta=0.5))
	    	areaRoc.append(roc_auc_score(y_test, y_score[:,1]))
		

		#Generate ROC curve for each cross-validation
	    	fpr, tpr, thresholds = roc_curve(y_test, y_score[:,1], pos_label = 1)  #Pos level for positive class
	    	precision, recall, threshold = precision_recall_curve(y_test, y_score[:,1])
	   	random_mean_tpr_10 += interp(random_mean_fpr_10, fpr, tpr)
	    	random_mean_tpr_10[0] = 0.0
	    	roc_auc = auc(fpr, tpr)
		#k = k + 1
		train_score.append(clf.fit(X_train, y_train).score(X_train, y_train))
		test_score.append(clf.fit(X_test, y_test).score(X_test, y_test))
    #plt.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (k+1, roc_auc))
    #plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
    #print "Roc area", areaRoc, "mean AUC", np.mean(areaRoc)
	random_mean_tpr_10 /= 10
	random_mean_tpr_10[-1] = 1.0
	random_mean_auc_10 = auc(random_mean_fpr_10, random_mean_tpr_10)
    #plt.plot(mean_fpr, mean_tpr, 'k--',label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)
    
    #Print all the scores
    #print "cross validation scores are", scores, "\n"
    #print "accuracy predicted", accurate, "\n"
    #print "accuracy randomised", accurate_rand, "\n"
    #print "precision", prec, "\n"
    #print "Recall", rec, "\n"
    #Write the data in the output file
    #combined = zip(accurate, accurate_rand, prec, precRand, rec, recRand, fscore, fscoreRand, areaRoc)
	print "scores are:\n"
	print scores
	combined = zip(accurate, accurate_rand, prec, precRand, rec, recRand, fscore, fscoreRand, areaRoc)
	print "######################################################\n"
	print "1", len(accurate) , "\n"
	print "2", len(accurate_rand) , "\n"
	print "3", len(prec) , "\n"
	print "4", len(precRand) , "\n"
	print "#####################\n"
	print combined
	#f = open(out_folder + "/tss_dist_Feature_selected_top5_H3k27me3_H3k36me3_Random_forest_10foldAllData_output", 'w')
	f = open(out_folder + "/RF_allDatatsetoutput_10Fold.txt", 'w')
	f.write("Accuracy\tAccuracy_Rand\tPrecision\tPrecision_Rand\tRecall\tRecall_Rand\tFscore\tFscore_rand\tareaRoc\n")
	for i in combined:
		line = '\t'.join(str(x) for x in i)
		f.write(line + '\n')
	string = str(np.mean(accurate)) + "\t" + str(np.mean(accurate_rand)) + "\t" + str(np.mean(prec)) + "\t" + str(np.mean(precRand)) + "\t" + str(np.mean(rec)) + "\t" + str(np.mean(recRand)) + "\t" + str(np.mean(fscore)) + "\t" + str(np.mean(fscoreRand)) + "\t" + str(np.mean(areaRoc))
	#string = str(np.mean(accurate)) + "\t"
	print "string", string
	f.write(string + '\n')
#Get varience across the cross-valiation scores:
	f.write ("Mean score 10 fold: {0:.3f} (+/-{1:.3f})".format(np.mean(scores), sem(scores)) + '\n')
	f.write ("Mean train score: {0:.3f} (+/-{1:.3f})".format(np.mean(train_score), sem(train_score)) + '\n')
	f.write ("Mean test score: {0:.3f} (+/-{1:.3f})".format(np.mean(test_score), sem(test_score)) + '\n')
	f.close()
	print "Roc area", areaRoc, "mean AUC", np.mean(areaRoc)
	plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
	plt.plot(random_mean_fpr_10, random_mean_tpr_10, 'k--',label='RF_ROC (area = %0.2f)' % random_mean_auc_10, lw=2, color=(0.45, 0.42, 0.18)) #Plot mean ROC area in cross validation
	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('Receiver operating characteristic example')
	plt.legend(loc="lower right")

	pl.savefig(out_folder + '/RF_allDatatsetoutput_10Fold.svg', bbox_inches='tight', pad_inches=0.2)
	#pl.savefig('/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/RF_ROC_all_data_less_data.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	#pl.savefig('/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Poeya_RF_ROC_all_data_less_data.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	plt.show()


if __name__ == "__main__":
    main(sys.argv[1:])


	

