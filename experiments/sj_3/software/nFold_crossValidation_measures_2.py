# Perform n-fold cross validation on the training dataset
# Date: 25/07/2015
# Author: Shalu Jhanwar
#!/usr/bin/python

import os
import sys
import numpy as np
import random
from scipy.stats import sem
import matplotlib.pyplot as plt
from scipy import interp
from sklearn import preprocessing
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn import cross_validation
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import fbeta_score
import optparse

def main(options, args):

    if len(sys.argv[1:]) == 0:
        print "no argument given!"
        print parser.print_help()
        sys.exit(2)
    if not options.data_file:
        print "No Data File Present \n Aborting....."
        sys.exit(2)
    out_folder = options.output_folder
    data_cols = options.data_columns
    label_col = int(options.label_column)

    if int(options.verbosity) > 0:
        print "Options: ", options
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
        print "output_folder is", options.output_folder, "\n"

    # Get columns indices of the features
    data_cols = [int(x) for x in data_cols.split(",")]

    # Read features and labels
    feature_data = np.loadtxt(options.data_file, usecols=data_cols, delimiter = "\t", skiprows = 1)
    labels = np.genfromtxt(options.data_file,  usecols=label_col, delimiter = "\t", skiprows = 1)

    #Perform same scaling on training and testing data
    scaler = preprocessing.StandardScaler()
    feature_data_scaled = scaler.fit_transform(feature_data)

    np.random.seed(0)
    shuffled_indices = np.random.permutation(len(feature_data_scaled))
    feature_data_shuffled = feature_data_scaled[shuffled_indices]
    labels_shuffled = labels[shuffled_indices]

    #Define lists to store ML measures
    scores = list()
    train_score = list()    #train_score
    test_score=list()   #test_score
    accurate = list()   #Accuracy
    prec = list()   #Precision
    rec = list()    #Recall
    areaRoc = list()    #AUC
    fscore = list() #F-measure

    #For random classification
    accurateRand = list()
    recRand = list()
    precRand = list()
    fscoreRand = list()

    #For Plotting ROC-curve
    random_mean_tpr_nFold = 0.0
    random_mean_fpr_nFold = np.linspace(0, 1, 100)
    random.seed(80)

    #Generate object for n-fold CV
    skf=cross_validation.StratifiedKFold(labels_shuffled, n_folds=int(options.fold_cross_validation))
    for feature_train_index, feature_test_index in skf:
        rand_list = list()
        features_train, features_test = feature_data_shuffled[feature_train_index], feature_data_shuffled[feature_test_index]
        labels_train, labels_test = labels_shuffled[feature_train_index], labels_shuffled[feature_test_index]

        if int(options.verbosity) > 0:
            print features_train, features_test, "\n"

        #choose a method of your choice
        if options.method == "SVM":
            C = float(options.C)
            gamma = float(options.gamma)
            if int(options.verbosity) > 0:
                print "Method chosen is: SVM\n"
            estimator = svm.SVC(kernel='rbf',probability=True, C=C, gamma = gamma)

        else:
            n_estimators=int(options.n_estimators)
            max_depth = float(options.max_depth)
            if int(options.verbosity) > 0:
                print "Method chosen is: Random Forest\n"
            estimator = RandomForestClassifier(n_estimators=n_estimators, max_depth=max_depth)

        #calculate scores for the method selected
        scores.append(estimator.fit(features_train, labels_train).score(features_test, labels_test))
        y_pred = estimator.predict(features_test)
        predicted_label_score = estimator.predict_proba(features_test)
        predicted_label_score =np.around(predicted_label_score, decimals=2)
        accurate.append(accuracy_score(labels_test, y_pred))

        #Calculate accuracy for each cross-validation (classification + Random classification)
        for i in range(0,len(y_pred)):
            rand_list.append(random.randint(0, 1))
        y_rand = np.array(rand_list)
        accurateRand.append(accuracy_score(labels_test, y_rand))

        #Calculate precision, recall, fscore for each cross-validation (classification + Random classification)
        prec.append(precision_score(labels_test, y_pred, average='micro'))
        rec.append(recall_score(labels_test, y_pred, average='micro'))
        fscore.append(fbeta_score(labels_test, y_pred, average='micro', beta=0.5))
        precRand.append(precision_score(labels_test, y_rand, average='micro'))
        recRand.append(recall_score(labels_test, y_rand, average='micro'))
        fscoreRand.append(fbeta_score(labels_test, y_rand, average='micro', beta=0.5))
        areaRoc.append(roc_auc_score(labels_test, predicted_label_score[:,1]))

        #Generate ROC curve for each cross-validation
        fpr, tpr, thresholds = roc_curve(labels_test, predicted_label_score[:,1], pos_label = 1)  #Pos level for positive class
        random_mean_tpr_nFold += interp(random_mean_fpr_nFold, fpr, tpr)
        random_mean_tpr_nFold[0] = 0.0
        train_score.append(estimator.fit(features_train, labels_train).score(features_train, labels_train))
        test_score.append(estimator.fit(features_test, labels_test).score(features_test, labels_test))

    random_mean_tpr_nFold /= int(options.fold_cross_validation)
    random_mean_tpr_nFold[-1] = 1.0
    random_mean_auc_nFold = auc(random_mean_fpr_nFold, random_mean_tpr_nFold)

    if int(options.verbosity) > 0:
        print "scores are:\n", scores, "\n"

    combined_measures = zip(accurate, accurateRand, prec, precRand, rec, recRand, fscore, fscoreRand, areaRoc)
    if int(options.verbosity) > 0:
        print "######################################################\n"
        print "1", len(accurate) , "\n"
        print "2", len(accurateRand) , "\n"
        print "3", len(prec) , "\n"
        print "4", len(precRand) , "\n"
        print "#####################\n"
        #print combined_measures
        #print "output_folder is", options.output_folder, "\n"

    nFold_result_file = options.output_folder + "/" +"measures_" + options.save_file + ".txt"
    prediction_output = open(nFold_result_file, 'w')
    prediction_output.write("Accuracy\tAccuracy_Rand\tPrecision\tPrecision_Rand\tRecall\tRecall_Rand\tFscore\tFscore_rand\tareaRoc\n")
    for i in combined_measures:
        line = '\t'.join(str(x) for x in i)
        prediction_output.write(line + '\n')
    mean_measures = str(np.mean(accurate)) + "\t" + str(np.mean(accurateRand)) + "\t" + str(np.mean(prec)) + "\t" + str(np.mean(precRand)) + "\t" + str(np.mean(rec)) + "\t" + str(np.mean(recRand)) + "\t" + str(np.mean(fscore)) + "\t" + str(np.mean(fscoreRand)) + "\t" + str(np.mean(areaRoc))
    if int(options.verbosity) >0:
        print "All the average measures are: ", mean_measures, "\n"
    prediction_output.write(mean_measures + '\n')

    #Get varience across the cross-valiation scores:
    prediction_output.write ("Mean score int(options.fold_cross_validation) fold: {0:.3f} (+/-{1:.3f})".format(np.mean(scores), sem(scores)) + '\n')
    prediction_output.write ("Mean train score: {0:.3f} (+/-{1:.3f})".format(np.mean(train_score), sem(train_score)) + '\n')
    prediction_output.write ("Mean test score: {0:.3f} (+/-{1:.3f})".format(np.mean(test_score), sem(test_score)) + '\n')
    prediction_output.close()

    if int(options.verbosity)>0:
        print "Roc area", areaRoc, "mean AUC", np.mean(areaRoc)

    #Print ROC curve for n-fold cross-validation
    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
    plt.plot(random_mean_fpr_nFold, random_mean_tpr_nFold, 'k--',label='RF_ROC (area = %0.2f)' % random_mean_auc_nFold, lw=2, color=(0.45, 0.42, 0.18)) #Plot mean ROC area in cross validation
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC: fold_cross_validation fold CV')
    plt.legend(loc="lower right")

    #Save plot in svg format:
    ROC_curve_figure_file =  options.output_folder + "/" + "ROC-curve_" + options.save_file + ".svg"
    plt.savefig(ROC_curve_figure_file, bbox_inches='tight', pad_inches=0.2)
    plt.show()
    plt.close()

    #Defining subroutine:
if __name__ == "__main__":
    usage = "usage: %prog [required: --data-file <Filename> --feature-columns <feature column indices> --label-column <label column>] [optional: --output-folder <Output_folder> --fold-cross-validation <no. of cross-validation-folds> --save-file <output-filename> --n-estimators <value> --max-depth <value>]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("", "--data-file", dest="data_file", default="", help="A tab delimited file: 1st column as rownames, 2nd column as class and additional columns as tab-delimited features: see example folder for test file")
    parser.add_option("", "--output-folder", dest="output_folder", default="../output/", help="An output folder: Default is ../output")
    parser.add_option("", "--label-column", dest="label_column", default=1, help="Column index containing class of instances: Default: 1 (2nd column)")
    parser.add_option("", "--feature-columns", dest="data_columns", default="2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17", help="Column index containing features in data-file")
    parser.add_option("", "--fold-cross-validation", dest="fold_cross_validation", default=10, help="n-fold: Default:10")
    parser.add_option("", "--save-file", dest="save_file", default="output_file", help="Output filename: Deafult: output_file")
    parser.add_option("", "--n-estimators", dest="n_estimators", default=100, help="n_estimator list: Default: 100")
    parser.add_option("", "--n-jobs", dest="n_jobs", default=10, help="no. of CPUs: Default:10")
    parser.add_option("", "--C", dest="C", default=100000000.0, help="C <value>: Default: 100000000.0")
    parser.add_option("", "--gamma", dest="gamma", default=0.01, help="gamma: <value> default: 0.01")
    parser.add_option("", "--method", dest="method", default="RF", help="Method: 'RF': Random Forest, 'SVM': Support Vector Machine: Default: RF")
    parser.add_option("", "--max-depth", dest="max_depth", default=5, help="max_depth=<value>: Default:5")
    parser.add_option("", "--verbosity", dest="verbosity", default=1, help="Verbosity: Default on: provide 0 to turn off")
    (options, args) = parser.parse_args()
    main(options, args)

	

