#!/usr/bin/python

import sys
import getopt
import numpy as np
import pylab as pl
import random
from sklearn import datasets
from scipy import interp
import matplotlib.pyplot as plt
from scipy import interp
from sklearn import svm
from sklearn import preprocessing
from sklearn import linear_model
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.svm import SVC
from sklearn.metrics import roc_auc_score
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import fbeta_score
from sklearn.ensemble import ExtraTreesClassifier
import os
os.getcwd()

def main(argv):


    # get options passed at command line

	try:
		opts, args = getopt.getopt(argv, "d:D:c:C:g:p:")

	except getopt.GetoptError:

        #print helpString

		sys.exit(2)
#print opts
	for opt, arg in opts:

		if opt == '-d':

			data_file = arg

		elif opt == '-D':

			out_folder = arg

		elif opt == '-c':

			label_col = int(arg)

		elif opt == '-C':

			data_cols = arg

		elif opt == '-g':
	
			g_val = float(arg)

		elif opt == '-p':

			C_val = int(arg)
	
	
	data_cols = [int(x) for x in data_cols.split(",")]
	x_data = np.loadtxt(data_file, usecols=data_cols, delimiter = "\t")
	y_data = np.genfromtxt(data_file,  usecols = label_col, delimiter = "\t")

	#Scaling of the data
	#min_max_scaler = preprocessing.MinMaxScaler()
	#x_data = min_max_scaler.fit_transform(x_data)
	
	#same Scaling on both test and train data (centering the data scaling)
	scaler = preprocessing.StandardScaler()
	x_data = scaler.fit_transform(x_data)

	np.random.seed(0)
	indices = np.random.permutation(len(x_data))
	My_X_train = x_data[indices]
	My_Y_train = y_data[indices]


	# dataset for decision function visualization
	#X_2d = X[:, :2]
	#X_2d = X_2d[Y > 0]
	#Y_2d = Y[Y > 0]
	#Y_2d -= 1

	#Scaling between (0,1)
	#min_max_scaler = preprocessing.MinMaxScaler()
	#X_2d = min_max_scaler.fit_transform(X_2d)
	#X_2d

	#np.random.seed(0)
	#indices = np.random.permutation(len(X_2d))
	#My_X_train = X_2d[indices]
	#My_Y_train = Y_2d[indices]

	# It is usually a good idea to scale the data for SVM training.
	# We are cheating a bit in this example in scaling all of the data,
	# instead of fitting the transformation on the training set and
	# just applying it on the test set.

	#It is usually a good idea to scale the data for SVM training.
	# We are cheating a bit in this example in scaling all of the data,
	# instead of fitting the transformation on the training set and
	# just applying it on the test set. Centering and scaling happen independently on each feature by computing the relevant statistics on the samples in the training set. Mean and standard deviation are then stored to
	#be used on later data using the transform method.
	#As I want to scale the columns between 0 to 1 so I
	#am using preprocessing.MinMaxScaler() scalar rather than statndard scalar


	##############################################################################
	# Train classifier
	#
	# For an initial search, a logarithmic grid with basis
	# 10 is often helpful. Using a basis of 2, a finer
	# tuning can be achieved but at a much higher cost.
	#10 fold cross validation in the absence of the test set
	
	scores = list()
	accurate = list()
	prec = list()
	rec = list()
	areaRoc = list()
	fscore = list()
	#For random classification
	accurate_rand = list()
	recRand = list()
	precRand = list()
	fscoreRand = list()
	areaRocRand = list()

	##For SVM Radial
	RBF_mean_tpr = 0.0
	RBF_mean_fpr = np.linspace(0, 1, len(x_data))
	RBF_all_tpr = []
	k=1
	
	skf=cross_validation.StratifiedKFold(My_Y_train, n_folds=10)
    	for train_index, test_index in skf:
	    rand_list = list()
            X_train, X_test = My_X_train[train_index], My_X_train[test_index]
	    y_train, y_test = My_Y_train[train_index], My_Y_train[test_index]
	    print X_train, X_test, "\n" 
	#Fit the model woth best parameters
	    rbf_svc = svm.SVC(kernel='rbf',probability=True, C=C_val, gamma = g_val)
	    #rbf_svc = svm.SVC(kernel='rbf',probability=True)
	#scores.append(rbf_svc.fit(X_train, y_train).score(X_test, y_test))
	    scores.append(rbf_svc.fit(X_train, y_train).score(X_test, y_test))
	    print "train", len(X_train)
	    print "ytrain", len(y_train)
	    print "X_test", len(X_test)
	    print "y_test", len(y_test)
	#Perform model evaluation by calculating the accuracy
	#For RBF
	    y_pred = rbf_svc.predict(X_test)
	    y_score = rbf_svc.predict_proba(X_test)
	    y_score =np.around(y_score, decimals=2)
	    accurate.append(accuracy_score(y_test, y_pred))
	    print "accurate", accurate
	#Perform random classification score for binary values only
	    random.seed(k)
	    for i in range(0,len(y_pred)):
		rand_list.append(random.randint(0, 1))
	    y_rand = np.array(rand_list)
	    print "y_rand", y_rand, "k", k
	    #Calculate accuracy by generating random classification score
	    accurate_rand.append(accuracy_score(y_test, y_rand))
	    print "accurate_rand", accurate_rand
	    
	    #Calculate precision, recall, fscore For each cross-validation (classification + Random classification)
	    prec.append(precision_score(y_test, y_pred, average='micro'))
	    rec.append(recall_score(y_test, y_pred, average='micro'))
	    fscore.append(fbeta_score(y_test, y_pred, average='micro', beta=0.5))
	    precRand.append(precision_score(y_test, y_rand, average='micro'))
	    recRand.append(recall_score(y_test, y_rand, average='micro'))
	    fscoreRand.append(fbeta_score(y_test, y_rand, average='micro', beta=0.5))
	    areaRoc.append(roc_auc_score(y_test, y_score[:,1]))
	    fpr, tpr, thresholds = roc_curve(y_test, y_score[:,1], pos_label = 1)  #Pos level for positive class
	    precision, recall, threshold = precision_recall_curve(y_test, y_score[:,1])
	    RBF_mean_tpr += interp(RBF_mean_fpr, fpr, tpr)
	    RBF_mean_tpr[0] = 0.0
	    roc_auc = auc(fpr, tpr)
	#plt.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (k+1, roc_auc))
	#plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
	    k = k + 1
	print "Roc area", areaRoc, "mean AUC", np.mean(areaRoc)
	RBF_mean_tpr /= 10
	RBF_mean_tpr[-1] = 1.0
	RBF_mean_auc = auc(RBF_mean_fpr, RBF_mean_tpr)
	#Print all the scores
	print "cross validation scores are", scores, "\n"
	print "accuracy predicted", accurate, "\n"
	print "accuracy randomised", accurate_rand, "\n"
	#print "precision", prec, "\n"
	#print "Recall", rec, "\n"
	#Write the data in the output file
	combined = zip(accurate, accurate_rand, prec, precRand, rec, recRand, fscore, fscoreRand, areaRoc)
	print "######################################################\n"
	print "1", len(accurate) , "\n"
	print "2", len(accurate_rand) , "\n"
	print "3", len(prec) , "\n"
	print "4", len(precRand) , "\n"
	print "#####################\n"
	print combined
	f = open(out_folder + "/Poeya_Tune_scale_Radial_feature_selected_AllDataset_SVM_output", 'w')
	#f = open("/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Poeya_Radial_notune_noscale_SVM_output", 'w')
	f.write("Accuracy\tAccuracy_Rand\tPrecision\tPrecision_Rand\tRecall\tRecall_Rand\tFscore\tFscore_rand\tareaRoc\n")
	for i in combined:
		line = '\t'.join(str(x) for x in i)
	    	f.write(line + '\n')
	string = str(np.mean(accurate)) + "\t" + str(np.mean(accurate_rand)) + "\t" + str(np.mean(prec)) + "\t" + str(np.mean(precRand)) + "\t" + str(np.mean(rec)) + "\t" + str(np.mean(recRand)) + "\t" + str(np.mean(fscore)) + "\t" + str(np.mean(fscoreRand)) + "\t" + str(np.mean(areaRoc))
	#string = str(np.mean(accurate)) + "\t"
	print "string", string
	f.write(string + '\n')
	f.close()
	print "Roc area", areaRoc, "mean AUC", np.mean(areaRoc)
	#Generate ROC curve to compare the classifiers#

	plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Standard')
	plt.plot(RBF_mean_fpr, RBF_mean_tpr, 'k--',label='RBF_ROC (area = %0.2f)' % RBF_mean_auc, lw=2, color=(0.45, 0.42, 0.18)) #Plot mean ROC area in cross validation

	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('Receiver operating characteristic example')
	plt.legend(loc="lower right")
	pl.savefig(out_folder + '/Poeya_Radial_TUNE_feature_selected_AllDataset_SVM_ROC_robust.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	#pl.savefig('/no_backup/GD/resource/human/hg19/Encode/K562_Encode/K562_encode_updated/Poeya_Radial_notune_noscale_SVM_output.svg', transparent=True, bbox_inches='tight', pad_inches=0.2)
	plt.show()


if __name__ == "__main__":
	main(sys.argv[1:])
